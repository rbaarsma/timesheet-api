if ! [ "$1" == "--env=test" ]; then
	echo "Environment? (test)"
        read ENV
fi;

if ! [ "$ENV" == "dev" ]; then
	E="test"
else
	E=$ENV
fi;

php bin/console doc:database:drop --force --env=$E
php bin/console doc:database:create --env=$E
php bin/console doc:schema:update --force --env=$E
php bin/console app:fixtures:load --env=$E
