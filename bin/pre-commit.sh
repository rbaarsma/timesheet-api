#!/bin/sh

vendor/bin/php-cs-fixer fix --diff --level=symfony --fixers=align_double_arrow,short_array_syntax,align_equals,header_comment src/

exit $?