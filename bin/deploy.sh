#!/bin/bash
# Deployment helper

# skip everything with -y option
if ! [ "$1" == "-y" ]; then
	echo "Environment? (dev)"
	read ENV

	echo "Install Composer? (y/N)"
	read COMPOSER

	echo "Create Databases? (y/N)"
	read DATABASE_CREATE

	echo "Update database schema? (Y/n)"
	read DATABASE_UPDATE

    if ! test -e "./var/jwt/private.pem"; then
        mkdir -p var/jwt
        openssl genrsa -out var/jwt/private.pem -aes256 4096
        openssl rsa -pubout -in var/jwt/private.pem -out var/jwt/public.pem
    fi
    if [ "$COMPOSER" == "y" ] || [  "$COMPOSER" == "Y" ]; then
        composer install
    fi
    if [ "$DATABASE_CREATE" == "y" ] || [  "$DATABASE_CREATE" == "Y" ]; then
        php bin/console doc:database:drop --force --env=$ENV
        php bin/console doc:database:create --env=$ENV
        php bin/console doc:schema:update --force --env=$ENV
        php bin/console app:fixtures:load --env=$ENV
    fi

    if ! test -e ".git/hooks/pre-commit"; then
        ln -s bin/pre-commit.sh .git/hooks/pre-commit
        chmod +x .git/hooks/pre-commit
    fi
fi

if ! test -e "./phpunit.xml" ; then
	cp ./phpunit.xml.dist ./phpunit.xml
fi

if ! [ "$DATABASE_UPDATE" == "n" ] && ! [  "$DATABASE_UPDATE" == "N" ]; then
    php bin/console doc:schema:update --force
fi

if ["$2" == "-p"] || [ "$ENV" == "prod" ]; then
    composer dump-autoload --optimize    
else
    php bin/console c:c
fi