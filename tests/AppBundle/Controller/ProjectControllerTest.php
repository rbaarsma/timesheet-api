<?php

namespace Tests\AppBundle\Controller;

use AppBundle\AppTestCase;

/**
 * Full test scenario for Project 
 *
 * @covers CustomerController::allAction
 * @covers CustomerController::getAction
 * @covers CustomerController::postAction
 * @covers CustomerController::patchAction
 * @covers CustomerController::deleteAction
 */
class ProjectControllerTest extends AppTestCase
{
    public function testCompleteScenario()
    {
        $client = $this->getClient();
        
        // login as user
        $this->login('user@localhost');
        
        // GET is authorized, but probably nothing there
        $data = $this->jsonRequest('GET', '/project/');

        // POST, PATCH and DELETE are unauthorized for user
        $this->jsonRequest("POST", "/project/", null, 403);
                
        // GET single is authorized, but no response
        $this->jsonRequest("GET", "/project/99", null, 404);
        
        // login as admin
        $this->login('admin@localhost');

        // first GET might be empty, but doesn't need to be, simply check if is array
        $data = $this->jsonRequest('GET', '/project/');
        $this->assertTrue(is_array($data));
        $count = count($data);

        // test when doesn't exist
        $this->jsonRequest("PATCH", "/project/99", null, 404);
        $this->jsonRequest("DELETE", "/project/99", null, 404);
        
        // POST new project
        $data = $this->jsonRequest('POST', '/project/', [
            'project'=>[
                'name' => 'Autotest'.mt_rand(1,100000),
                'customer' => 1
                // more fields...
            ]
        ]);
        $this->assertTrue(is_object($data));
        $this->assertGreaterThan(0, $data->id);
        $id = $data->id;
        
        // quick test if a normal user cannot patch or delete
        $this->login('user@localhost');
        $this->jsonRequest("PATCH", "/project/$id", null, 403);
        $this->jsonRequest("DELETE", "/project/$id", null, 403);
        $this->login('admin@localhost');
        
        // GET all projects (this time should have results)
        $data = $this->jsonRequest('GET', '/project/');
        $this->assertGreaterThan($count, count($data));
        $last = end($data);
        $this->assertTrue(is_object($last));
        //$this->assertEquals($id, $last->id);

        // GET single by id
        $data = $this->jsonRequest('GET', "/project/$id");
        $this->assertTrue(is_object($data));
        //$this->assertEquals($id, $data->id);

        // PATCH single by id
        $data = $this->jsonRequest('PATCH', "/project/$id", [
            'project'=>[
                'name' => 'Changedtest'.mt_rand(1,100000),
                'customer' => 1
                // more fields...
            ]
        ]);
        $this->assertTrue(is_object($data));
        //$this->assertEquals($id, $data->id);
        
        // POST new project on wrong organization
        $data = $this->jsonRequest('POST', '/project/', [
            'project'=>[
                'name' => 'Test',
                'customer' => '1',
                'organization' => '2'
                // more fields...
            ]
        ], 400);
        
        // POST new project on wrong customer
        $this->jsonRequest('POST', '/project/', [
            'project'=>[
                'name' => 'Test2',
                'customer' => '2'
                // more fields...
            ]
        ], 400);
        
        // GET, PATCH, DELETE single by object id tied to wrong organization
        $this->jsonRequest('GET', "/project/2", null, 403);
        $this->jsonRequest('DELETE', "/project/2", null, 403);
        $this->jsonRequest("PATCH", "/project/2", null, 403);
        
        // PATCH single by id into wrong customer
        $this->jsonRequest('PATCH', "/project/$id", [
            'project'=>[
                'name' => 'Changedtest'.mt_rand(1,100000),
                'customer' => 2
                // more fields...
            ]
        ], 400);
        
        // DELETE single by id
        $data = $this->jsonRequest('DELETE', "/project/$id");
        $this->assertTrue($data);
    }
}
