<?php

namespace Tests\AppBundle\Controller;

use AppBundle\AppTestCase;

/**
 * Full test scenario for User 
 *
 * @covers CustomerController::allAction
 * @covers CustomerController::getAction
 * @covers CustomerController::postAction
 * @covers CustomerController::patchAction
 * @covers CustomerController::deleteAction
 */
class UserControllerTest extends AppTestCase
{
    public function testCompleteScenario()
    {
        $client = $this->getClient();
        
        // --- simple tests if admin stuff is forbidden to normal user ---

        // login as user
        $this->login('user@localhost');
                
        $this->jsonRequest('GET', '/user/', null, 403);
        $this->jsonRequest("POST", "/user/", null, 403);
        $this->jsonRequest("PATCH", "/user/1", null, 403);
        $this->jsonRequest("DELETE", "/user/1", null, 403);
        $this->jsonRequest("GET", "/user/1", null, 403);
                
        // login as admin
        $this->login('admin@localhost');
        
        // --- test stuff with admin ---

        // first GET might be empty, but doesn't need to be, simply check if is array
        $data = $this->jsonRequest('GET', '/user/');
        $this->assertTrue(is_array($data));
        
        // delete old test data if not done so already
        foreach ($data as $key=>$user) {
            if ($user->email === 'test01@solidwebcode.com' || $user->email === 'test02@solidwebcode.com') {
                $this->jsonRequest("DELETE", "/user/".$user->id);
                unset($data[$key]);
            }
        }
        
        // remember count for after POST
        $count = count($data);

        // test when doesn't exist
        $this->jsonRequest("PATCH", "/user/9999", null, 404);
        $this->jsonRequest("DELETE", "/user/9999", null, 404);
        
        // POST new
        $data = $this->jsonRequest('POST', '/user/', [
            'user'=>[
                'email' => 'test01@solidwebcode.com',
                'plainPassword' => '1234',
                'enabled' => true,
                'roles' => [],
            ]
        ]);
        $this->assertTrue(is_object($data));
        $this->assertEquals('test01@solidwebcode.com', $data->email);
        $this->assertEquals(false, $data->locked);
        $this->assertEquals(['ROLE_USER'], $data->roles);
        $id = $data->id;
        
        // try to login as new user
        $this->login('test01@solidwebcode.com', '1234');
        $this->login('admin@localhost');
        
        // GET all (this time should have results)
        $data = $this->jsonRequest('GET', '/user/');
        $this->assertGreaterThan($count, count($data));
        $last = end($data);
        $this->assertTrue(is_object($last));
        $this->assertEquals('test01@solidwebcode.com', $last->email);
        $this->assertEquals($id, $last->id);

        // GET single by id
        $data = $this->jsonRequest('GET', "/user/$id");
        $this->assertTrue(is_object($data));
        $this->assertEquals('test01@solidwebcode.com', $data->email);
        $this->assertEquals($id, $data->id);

        // PATCH single by id
        $data = $this->jsonRequest('PATCH', "/user/$id", [
            'user'=>[
                'email' => 'test02@solidwebcode.com',
                'plainPassword' => 'Test@1234',
                'enabled' => false,
                'roles' => ['ROLE_MANAGER'],
            ]
        ]);
        $this->assertTrue(is_object($data));
        $this->assertEquals('test02@solidwebcode.com', $data->email);
        $this->assertEquals(false, $data->enabled);
        $this->assertEquals(['ROLE_MANAGER','ROLE_USER'], $data->roles);
        
        // login should be blocked now
        $client->request('POST', '/api/user/login_check', [
            '_username' => 'test02@solidwebcode.com',
            '_password' => 'Test@1234',
        ]);
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent());
        $this->assertEquals('Bad credentials', $data->message);
        
        // PATCH single by id
        $data = $this->jsonRequest('PATCH', "/user/$id", [
            'user'=>[
                //'email' => '',
                'plainPassword' => 'Test@1234',
                'enabled' => true,
                'roles' => ['ROLE_MANAGER'],
            ]
        ]);
        $this->assertTrue(is_object($data));
        $this->assertEquals('test02@solidwebcode.com', $data->email);
        $this->assertEquals(true, $data->enabled);
        $this->assertEquals(['ROLE_MANAGER','ROLE_USER'], $data->roles);
        
        $this->login('test02@solidwebcode.com', 'Test@1234');
        $this->login('admin@localhost');
        
        // test form validation        
        $data = $this->jsonRequest('PATCH', "/user/$id", [
            'user'=>[
                'email' => 'noemail',
                //'locked' => "wut?",
                'roles' => ['ROLE_ADMIN'],
            ]
        ], 400);
        $this->assertEquals(1, count($data->errors->children->roles->errors));

        // DELETE single by id
        $data = $this->jsonRequest('DELETE', "/user/$id");
        $this->assertTrue($data);
    }
    
    /**
     * @covers UserController::registerAction
     */
    public function testRegister()
    {
        // make sure not logged in
        $this->client = static::createClient();
        
        // remove old registration
        $em = $this->client->getContainer()->get('doctrine')->getManager();
        if ($organization = $em->getRepository('AppBundle:Organization')->findOneByName('Test')) { 
            $em->remove($organization); 
            $em->flush(); // should cascade
        }
            
        // POST new
        $data = $this->jsonRequest('POST', '/user/register', [
            'user'=>[
                'email' => 'register@solidwebcode.com',
                'plainPassword' => '1234',
                //'enabled' => true,
                //'roles' => [],
            ],
            'organization'=>[
                'name' => 'Test',
                'package' => 'basic'
            ]
        ]);

        // check if user is indeed admin/enabled
        $this->assertEquals('register@solidwebcode.com', $data->email);
        $this->assertTrue($data->enabled);
        $this->assertTrue(in_array('ROLE_ADMIN', $data->roles));
    }
    
    /**
     * @covers UserController::sendResetAction
     * @covers UserController::resetAction
     */
    public function testResetPassword()
    {
        // make sure not logged in
        $this->client = static::createClient();

        // invalid token
        $data = $this->jsonRequest('POST', '/user/reset', [
            'token' => sha1(uniqid("",true)),
            'plainPassword' => '1234'
        ], 404);
        
        // test invalid e-mail
        $data = $this->jsonRequest('POST', '/user/send-reset', [
            'email' => 'doesnotexist@localhost.com'
        ], 404);
        
        // actual success situation
        $this->client->enableProfiler();
        
        // POST send-reset
        $data = $this->jsonRequest('POST', '/user/send-reset', [
            'email' => 'register@solidwebcode.com'
        ]);
        $this->assertTrue($data);
        
        $mailCollector = $this->client->getProfile()->getCollector('swiftmailer');
        $this->assertEquals(1, $mailCollector->getMessageCount());
        
        $collectedMessages = $mailCollector->getMessages();
        $message = $collectedMessages[0];
        $this->assertEquals('Password Reset', $message->getSubject());
        
        $matches = [];
        preg_match('@href="http://www.solidtimesheet.com/reset-password/(.*)">@', $message->getBody(), $matches);        
        $token = $matches[1];
        
        $data = $this->jsonRequest('POST', '/user/reset', [
            'token' => $token,
            'plainPassword' => '1234'
        ]);
        $this->token = $data->token;
        
        // $data should be user
        $this->assertEquals('register@solidwebcode.com', $data->user->email);
        $this->assertTrue($data->user->enabled);
        
        // should be logged in
        $this->jsonRequest('GET', '/user/');
    }
}
