<?php

namespace Tests\AppBundle\Controller;

use AppBundle\AppTestCase;

/**
 * Full test scenario for Hour 
 *
 * @covers CustomerController::allAction
 * @covers CustomerController::getAction
 * @covers CustomerController::postAction
 * @covers CustomerController::patchAction
 * @covers CustomerController::deleteAction
 */
class HourControllerTest extends AppTestCase
{
    
    public function testCompleteScenario()
    {
        $client = $this->getClient();
        
        // login as user
        $this->login('user@localhost');
        
        // GET is authorized, but probably nothing there
        $data = $this->jsonRequest('GET', '/hour/');
                
        // GET single is authorized, but no response
        $this->jsonRequest("GET", "/hour/9999", null, 404);
        
        // login as admin
        $this->login('admin@localhost');

        // first GET might be empty, but doesn't need to be, simply check if is array
        $data = $this->jsonRequest('GET', '/hour/');
        $this->assertTrue(is_array($data));
        $count = count($data);

        // test when doesn't exist
        $this->jsonRequest("PATCH", "/hour/9999", null, 404);
        $this->jsonRequest("DELETE", "/hour/9999", null, 404);
        
        // POST new hour
        $data = $this->jsonRequest('POST', '/hour/', [
            'hour'=>[
                'hours' => '6',
                'date' => '2016-02-23',
                'project' => '1',
                'customer' => '1',
                'comment' => 'Blaat',
                // more fields...
            ]
        ]);
        $this->assertTrue(is_object($data));
        $this->assertEquals('6', $data->hours);
        $this->assertEquals('23-02-2016', $data->date);
        $this->assertEquals('1', $data->project->id);
        $this->assertEquals('1', $data->customer->id);
        $this->assertEquals('Blaat', $data->comment);
        $this->assertGreaterThan(0, $data->id);
        $id = $data->id;
        
        // GET all hours (this time should have results)
        $data = $this->jsonRequest('GET', '/hour/');
        $this->assertGreaterThan($count, count($data));
        $last = end($data);
        $this->assertTrue(is_object($last));
        $this->assertEquals($last->hours, '6');
        $this->assertEquals($id, $last->id);
        
        // GET month view
        $data = $this->jsonRequest('GET', '/hour/month/2016/2');
        $this->assertGreaterThan(0, count($data));
        $this->assertEquals(23, $data[0][0]);
        $this->assertGreaterThan(5, $data[0][1]);
                
        // quick test if a normal user cannot patch or delete someone else's hour
        $this->login('user@localhost');
        $this->jsonRequest('PATCH', "/hour/$id", [
            'hour'=>[
                'hours' => '7',
                'date' => '2016-02-24',
                'project' => '1',
                'customer' => '1',
                'comment' => 'Blaat1',
                // more fields...
            ]
        ], 403);
        $this->jsonRequest("DELETE", "/hour/$id", null, 403);
        $this->login('admin@localhost');

        // GET single by id
        $data = $this->jsonRequest('GET', "/hour/$id");
        $this->assertTrue(is_object($data));
        $this->assertEquals('6', $data->hours);
        $this->assertEquals($id, $data->id);

        // PATCH single by id
        $data = $this->jsonRequest('PATCH', "/hour/$id", [
            'hour'=>[
                'hours' => '7',
                'date' => '2016-02-24',
                'project' => '1',
                'customer' => '1',
                'comment' => 'Blaat1',
                // more fields...
            ]
        ]);
        $this->assertTrue(is_object($data));
        $this->assertEquals('7', $data->hours);
        $this->assertEquals('24-02-2016', $data->date);
        $this->assertEquals('1', $data->project->id);
        $this->assertEquals('1', $data->customer->id);
        $this->assertEquals('Blaat1', $data->comment);
        $this->assertGreaterThan(0, $data->id);
        $id = $data->id;

        // DELETE single by id
        $data = $this->jsonRequest('DELETE', "/hour/$id");
        $this->assertTrue($data);
        
        // POST new hour on wrong organization
        $data = $this->jsonRequest('POST', '/hour/', [
            'hour'=>[
                'hours' => '7',
                'date' => '2016-02-24',
                'project' => '1',
                'customer' => '1',
                'comment' => 'Blaat99',
                'organization' => '2',
                // more fields...
            ]
        ], 400);
        
        // POST new hour on wrong customer
        $data = $this->jsonRequest('POST', '/hour/', [
            'hour'=>[
                'hours' => '7',
                'date' => '2016-02-24',
                'project' => '1',
                'customer' => '2',
                'comment' => 'Blaat99',
                'organization' => '1',
                // more fields...
            ]
        ], 400);
        
        // POST new hour on wrong project
        $data = $this->jsonRequest('POST', '/hour/', [
            'hour'=>[
                'hours' => '7',
                'date' => '2016-02-24',
                'project' => '2',
                'customer' => '1',
                'comment' => 'Blaat99',
                'organization' => '1',
                // more fields...
            ]
        ], 400);
        
        // GET, PATCH, DELETE single by object id tied to wrong organization
        $this->jsonRequest('GET', "/hour/2", null, 403);
        $this->jsonRequest('DELETE', "/hour/2", null, 403);
        $this->jsonRequest("PATCH", "/hour/2", null, 403);
        
        // --- test search ---
        
        // test without input
        $data = $this->jsonRequest('POST', '/hour/search', [
            'search'=>[]
        ], 200);
        $this->assertEquals($count, count($data));        
        
        // search for date that doesn't exist
        $data = $this->jsonRequest('POST', '/hour/search', [
            'search'=>[
                'date_from' => '2016-02-24',
                //'date_to' => '2016-02-24',
                //'project' => '2',
                //'customer' => '1',
                //'user' => '1',
                // more fields...
            ]
        ], 200);
        
        // search valid stuff
        $data = $this->jsonRequest('POST', '/hour/search', [
            'search'=>[
                'date_to' => '2016-02-24',
                'project' => '1',
                'customer' => '1',
            ]
        ], 200);
        $this->assertGreaterThan(1, count($data));
        
        // search for stuff from single user
        $data = $this->jsonRequest('POST', '/hour/search', [
            'search'=>[
                'user' => '4',
            ]
        ], 200);
        $this->assertGreaterThan(0, count($data));        
        
        // --- test choices ---
        $data = $this->jsonRequest('GET', '/hour/choices');
        $this->assertTrue(is_object($data));
        $this->assertGreaterThan(0, count($data->customers));
        $this->assertTrue(is_object($data->customers[0]));
        $this->assertGreaterThan(0, count($data->customers[0]->projects));
    }
}
