<?php

namespace Tests\AppBundle\Controller;

use AppBundle\AppTestCase;

/**
 * @covers CustomerController
 */
class CustomerControllerTest extends AppTestCase
{
    /**
     * @covers CustomerController::allAction
     * @covers CustomerController::getAction
     * @covers CustomerController::postAction
     * @covers CustomerController::patchAction
     * @covers CustomerController::deleteAction
     */
    public function testFull()
    {
        $client = $this->getClient();

        // test without login
        $this->jsonRequest("GET", "/customer/", null, 403);
        $this->jsonRequest("GET", "/customer/1", null, 403);
        $this->jsonRequest("POST", "/customer/", null, 403);
        $this->jsonRequest("PATCH", "/customer/1", null, 403);
        $this->jsonRequest("DELETE", "/customer/1", null, 403);
        
        // login as user
        $this->login('user@localhost');
        
        // GET is authorized, but probably nothing there
        $data = $this->jsonRequest('GET', '/customer/');

        // POST, PATCH and DELETE are unauthorized for user
        $this->jsonRequest("POST", "/customer/", null, 403);
                
        // GET single is authorized, but no response
        $this->jsonRequest("GET", "/customer/99", null, 404);
        
        // login as admin
        $this->login('admin@localhost');

        // first GET might be empty, but doesn't need to be, simply check if is array
        $data = $this->jsonRequest('GET', '/customer/');
        $this->assertTrue(is_array($data));
        $count = count($data);

        // test when doesn't exist
        $this->jsonRequest("PATCH", "/customer/99", null, 404);
        $this->jsonRequest("DELETE", "/customer/99", null, 404);
        
        // POST new customer
        $data = $this->jsonRequest('POST', '/customer/', [
            'customer'=>['name' => 'Autotest'.mt_rand(1,100000)]
        ]);
        $this->assertTrue(is_object($data));
        $this->assertGreaterThan(0, $data->id);
        $id = $data->id;
        
        // quick test if a normal user cannot patch or delete
        $this->login('user@localhost');
        $this->jsonRequest("PATCH", "/customer/$id", null, 403);
        $this->jsonRequest("DELETE", "/customer/$id", null, 403);
        $this->login('admin@localhost');
        
        // GET all customers (this time should have results)
        $data = $this->jsonRequest('GET', '/customer/');
        $this->assertGreaterThan($count, count($data));
        $last = end($data);
        $this->assertTrue(is_object($last));
        //$this->assertEquals($id, $last->id);

        // GET single by id
        $data = $this->jsonRequest('GET', "/customer/$id");
        $this->assertTrue(is_object($data));
        $this->assertEquals($id, $data->id);

        // PATCH single by id
        $data = $this->jsonRequest('PATCH', "/customer/$id", [
            'customer'=>['name' => 'Changedtest'.mt_rand(1,100000)]
        ]);
        $this->assertTrue(is_object($data));
        $this->assertEquals($id, $data->id);
        
        // DELETE single by id
        $data = $this->jsonRequest('DELETE', "/customer/$id");
        $this->assertTrue($data);
        
        // GET, PATCH, DELETE single by object id tied to wrong organization
        $this->jsonRequest('GET', "/customer/2", null, 403);
        $this->jsonRequest('DELETE', "/customer/2", null, 403);
        $this->jsonRequest("PATCH", "/customer/2", null, 403);
    }
}
