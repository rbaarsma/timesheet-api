<?php

namespace Tests\AppBundle\Controller;

use AppBundle\AppTestCase;

/**
 * Full test scenario for Organization 
 *
 * @covers CustomerController::allAction
 * @covers CustomerController::getAction
 * @covers CustomerController::postAction
 * @covers CustomerController::patchAction
 * @covers CustomerController::deleteAction
 */
class OrganizationControllerTest extends AppTestCase
{
    public function testCompleteScenario()
    {
        $client = $this->getClient();
        
        // login as user
        $this->login('user@localhost');
        
        // --- simple tests if admin stuff is forbidden to normal user ---
        
        // GET is unauthorized
        $data = $this->jsonRequest('GET', '/organization/', null, 403);

        // POST, PATCH and DELETE are unauthorized for user
        $this->jsonRequest("POST", "/organization/", null, 403);
                
        // GET single is authorized, but no response
        $this->jsonRequest("GET", "/organization/1", null, 403);
                
        // login as admin
        $this->login('super@localhost');
        
        // --- test stuff with admin ---

        // first GET might be empty, but doesn't need to be, simply check if is array
        $data = $this->jsonRequest('GET', '/organization/');
        $this->assertTrue(is_array($data));
        $count = count($data);

        // test when doesn't exist
        $this->jsonRequest("PATCH", "/organization/99", null, 404);
        $this->jsonRequest("DELETE", "/organization/99", null, 404);
        
        // POST new
        $data = $this->jsonRequest('POST', '/organization/', [
            'organization'=>[
                'name' => 'Test',
                'package' => 'basic'
                // more fields...
            ]
        ]);
        $this->assertTrue(is_object($data));
        $this->assertEquals('Test', $data->name);
        $this->assertGreaterThan(0, $data->id);
        $id = $data->id;
               
        // quick test if a normal user cannot patch or delete
        $this->login('user@localhost');
        $this->jsonRequest("GET", "/organization/$id", null, 403);
        $this->jsonRequest("PATCH", "/organization/$id", null, 403);
        $this->jsonRequest("DELETE", "/organization/$id", null, 403);
        $this->login('super@localhost');
        
        // GET all (this time should have results)
        $data = $this->jsonRequest('GET', '/organization/');
        $this->assertGreaterThan($count, count($data));
        $last = end($data);
        $this->assertTrue(is_object($last));
        $this->assertEquals($last->name, 'Test');
        $this->assertEquals($id, $last->id);

        // GET single by id
        $data = $this->jsonRequest('GET', "/organization/$id");
        $this->assertTrue(is_object($data));
        $this->assertEquals('Test', $data->name);
        $this->assertEquals($id, $data->id);

        // PATCH single by id
        $data = $this->jsonRequest('PATCH', "/organization/$id", [
            'organization'=>[
                'name' => 'Blaat',
                'package' => 'professional'
                // more fields...
            ]
        ]);
        $this->assertTrue(is_object($data));
        $this->assertEquals('Blaat', $data->name);
        $this->assertEquals($id, $data->id);

        // DELETE single by id
        $data = $this->jsonRequest('DELETE', "/organization/$id");
        $this->assertTrue($data);
    }
}