# Timesheet-api

### Prerequisites

* You'll need composer installed: http://symfony.com/doc/current/cookbook/composer.html
* You'll need to have PHP 5.5 or 5.6 installed. 

### Install

```bash
git clone https://rbaarsma@bitbucket.org/rbaarsma/timesheet-api.git
cd timesheet-api
composer install
```

At this point you're asked some questions.
* For the database stuff, check what database you're using.
* The first mail parameter should be "mail" (not smtp) and the rest the default
* The secret can be anything, doesn't matter
* The jwt key paths can be the default
* The jwt key passphrase should be something you choose, you'll need it in a moment
* The token ttl is the lifetime in seconds, you can just leave the default

Then run the deploy command, simply use the defaults and when the passphrase is
prompted, use the one you filled in before.

```bash
bin/deploy.sh
```

To get some base users and data, you should run the fixtures on environment=dev

```bash
bin/fixtures.sh 
```

Now everything should work and you can start the server:

```bash
php bin/console server:start
```

The server is now started and going to the root (http://localhost:8000/) will give you the api documentation.

### Troubleshooting

#### date.timzone missing (on mac)

Easy fix: 

```bash
vi /etc/php.ini
```

Add line:

```vi
date.timezone = Europe/Amsterdam
```

#### failing to connect to the database or failure to login (status 0)

If something doesn't work with the database or the login, you should check your *parameters.yml* and
check it's config. The *parameters.yml.dist* is the example file that has some extra comments.

### Using the API

If you go to http://localhost:8000/ you'll find yourself in the api documentation.

Because all the api's are secured, you'll first need a JWT security token. For this you can use the POST /api/login_check api (you can click to expand, should be the bottom one)

For now the system has 2 users, user@localhost // 1234 and admin@localhost // 1234. You can fill in either in the _username and _password values (behind String). Then use the button [Try].

You should get a JSON response in the Response Body. Something similar to:
```json

▿{
  "token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXUyJ9.eyJleHAiOjE0NTU1NTU2NDIsInVzZXJuYW1lIjoidXNlciIsImlhdCI6IjE0NTU0NjkyNDIifQ.ZewV3L_AcreGNKuIoQ_SxV39F6t-8ABhnXrdyQrE0ctaC9u2ehSlLwc2PdyNbXlqSX9ICjgyUR3BdZ_yCNFJPj22VdW7IJjwopC3foKeN9WaDGrfQ248IVO5lm_0yuAsrmThxkwVgZUVD0sKVPqOdOsK2yxZP3BjzCZxXtLGJG5zqbuJYVje4BxVXrA8aylnkAWAwaAY26q38ig_hy21o74da1zO-LhyDjcuwQafcjhVViyeay84RdJyjgLmngK2-AUcyrN3Kl2vmH6csPmE_jZmvgR3pphpq-te6mkxGYqIWVM7YNECFPuFGnFgjYm00_TkmMjBhcZnXnWUKZoiFwvcvXFOiGcEumaDspxRiHAUGBlzW_TzgHtyN5f4JiH7NbLTLl0QIPSwoG-D6c3upxYfc4lveyGp0d0Fkqa0QBPtkvh5caCFpVLc2v69PRMRnj1HG64ru9ZQyvE69EDGyZrVUARKdisHhTLNSNiRW1VcLzZZpua-_E3w9RCQyrEnl0APbVnsS1X4rNqQ6RGf2E1DBF7oIpTWQ2Pygua8V8o7unPn2YU6TemzhvQRa0mHuEYjT8AE39d9bN1Kit3sKOusJOSND7eJKdk1XDPMp4MnbjAK6O7ICi-Ad3Iyl_vSssuvg7dmL_dvm_sAFmGpEOCYvakLjLe3SboiO7_tXss"
}
```

Copy the token (in the example from eyJhbGc...tXss) and go to the /api/customer API. To authorize you'll need to add the correct header. 
Header-name: Authorization
Header-value: Bearer TOKEN
(obviously here you'd replace TOKEN with the actual token.)

If you've done everything correctly and push [Try] you should get a status=200 json response with only `[]`. This means you did well!

As a last example try and POST a new customer. If you open the POST /customer API, you'll notice the name field. In the value here add a random name. Also don't forget to add the same header. Authorization: Bearer TOKEN. If you did this correctly you'll get a status=200 and a json object with a name, something like:
```json
{
  "name": "Rein"
}
```

If you'd try the GET /customer api again, you should get this customer back in the array, something like
```json
[{ "name": "Rein"}]
```

Good luck with your further development!