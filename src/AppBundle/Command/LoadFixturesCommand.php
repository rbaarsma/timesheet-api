<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Create Fixtures.
 */
class LoadFixturesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:fixtures:load')
            ->setDescription('Load basic fixtures')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->write("Loading fixtures...\n");

        $path    = $this->getContainer()->get('kernel')->getRootDir().'/Resources/fixtures/fixtures.yml';
        $em      = $this->getContainer()->get('doctrine')->getManager();
        $objects = \Nelmio\Alice\Fixtures::load($path, $em);

        $persister = new \Nelmio\Alice\Persister\Doctrine($em);
        $persister->persist($objects);

        $output->write('Fixtures loaded successfully! Inserted '.count($objects)." objects.\n");
    }
}
