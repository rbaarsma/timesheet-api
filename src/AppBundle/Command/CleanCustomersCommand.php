<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Create Fixtures.
 */
class CleanCustomersCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:clean:customers')
            ->setDescription('Clean unused customers and projects')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->write("Cleaning customers and projects...\n");

        $conn = $this->getContainer()->get('doctrine')->getConnection();

        $sql = "DELETE c FROM customer c LEFT JOIN hour h ON c.id=h.customer_id WHERE h.id IS NULL";
        $deleted_customers = $conn->executeUpdate($sql);

        $sql = "DELETE p FROM project p LEFT JOIN hour h ON p.id=h.project_id WHERE h.id IS NULL";
        $deleted_projects = $conn->executeUpdate($sql);

        $output->write(sprintf("Deleted %d customers and %d projects.", $deleted_customers, $deleted_projects));
    }
}
