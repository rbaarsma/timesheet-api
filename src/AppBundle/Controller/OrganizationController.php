<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Entity\Organization;
use AppBundle\Form\OrganizationType;

/**
 * Organization controller.
 *
 * @Route("/organization")
 */
class OrganizationController extends FOSRestController
{
    /**
     * Lists all Organization entities.
     *
     * @Route("/")
     * @Method("GET")
     * @View()
     * @ApiDoc(
     *   description="GET all organizations"
     * )
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        /* @var $repository \AppBundle\Repository\OrganizationRepository */
        $repository = $em->getRepository('AppBundle:Organization');

        /* @var $organizations Organization[] */
        $organizations = $repository->findAll();

        return $organizations;
    }

    /**
     * Creates a new Organization entity.
     *
     * @Route("/")
     * @Method("POST")
     * @ApiDoc(
     *   description="POST organization",
     *   parameters={ 
     *      {"name"="organization[name]","dataType"="string","required"=true,"description"="Organization name"},
     *      {"name"="organization[package]","dataType"="string","required"=true,"description"="Package (free|support)"},
     *      {"name"="organization[phone]","dataType"="string","required"=false,"description"="(Optional) Phone number"},
     *      {"name"="organization[website]","dataType"="string","required"=false,"description"="(optional) Website url"}
     *   }
     * )
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function postAction(Request $request)
    {   
        $organization = new Organization();
        $form = $this->createForm(OrganizationType::class, $organization);

        $form->handleRequest($request);
        if (!$form->isSubmitted()) {
            throw new BadRequestHttpException("Form was not submitted. Data should be wrapped in formname (ex ['organization'=>['name'=>'test']]).");
        }
        if (!$form->isValid()) {
            return $form;
        }
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($organization);
        $em->flush();

        return $organization;
    }

    /**
     * Finds and displays a Organization entity.
     *
     * @Route("/{id}")
     * @Method("GET")
     * @ParamConverter()
     * @View()
     * @ApiDoc(
     *   requirements={ { "name"="id","dataType"="integer","requirement"="\d+","description"="id of organization" } },
     *   description="GET single organization by id"
     * )
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function showAction(Organization $organization)
    {

        return $organization;
    }

    /**
     * Displays a form to edit an existing Organization entity.
     *
     * @Route("/{id}")
     * @Method("PATCH")
     * @View()
     * @ApiDoc(
     *   requirements={ { "name"="id","dataType"="integer","requirement"="\d+","description"="id of organization" } },
     *   parameters={ 
     *      {"name"="organization[name]","dataType"="string","required"=true,"description"="Organization name"},
     *      {"name"="organization[package]","dataType"="string","required"=true,"description"="Package (free|support)"},
     *      {"name"="organization[phone]","dataType"="string","required"=false,"description"="(optional) Phone number"},
     *      {"name"="organization[website]","dataType"="string","required"=false,"description"="(optional) Website url"}
     *   },
     *   description="PATCH single organization by id"
     * )
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function patchAction(Request $request, Organization $organization)
    {
        $form = $this->createForm(OrganizationType::class, $organization, array('method' => 'PATCH'));

        $form->handleRequest($request);
        if (!$form->isSubmitted()) {
            throw new BadRequestHttpException("Form was not submitted. Data should be wrapped in formname (ex ['organization'=>['name'=>'test']]).");
        }
        if (!$form->isValid()) {
            return $form;
        }
        
        $em = $this->getDoctrine()->getManager();
        $em->flush();        

        return $organization;
    }

    /**
     * Deletes a Organization entity.
     *
     * @Route("/{id}")
     * @Method("DELETE")
     * @View()
     * @ApiDoc(
     *   requirements={ { "name"="id","dataType"="integer","requirement"="\d+","description"="id of organization" } },
     *   parameters={ {"name"="organization[name]","dataType"="string","required"=true,"description"="Organization name"} },
     *   description="DELETE single Organization by id"
     * )
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function deleteAction(Request $request, Organization $organization)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($organization);
        $em->flush();        

        return true;
    }
}
