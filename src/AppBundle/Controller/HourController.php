<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Form\Form;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Entity\Hour;
use AppBundle\Form\HourType;
use AppBundle\Form\SearchType;
use AppBundle\Entity\Customer;
use AppBundle\Entity\Project;
use AppBundle\Entity\Search;

/**
 * Hour controller.
 *
 * @Route("/hour")
 */
class HourController extends FOSRestController
{
    /**
     * Lists all Hour entities.
     *
     * @Route("/", name="hour_index")
     * @Method("GET")
     * @View()
     * @ApiDoc(
     *   description="GET all hours"
     * )
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        /* @var $repository AppBundle\Repository\HourRepository */
        $repository = $em->getRepository('AppBundle:Hour');

        /* @var $hours Hour[] */
        $hours = $repository->findByOrganization($this->getUser()->getOrganization());

        return $hours;
    }

    /**
     * Get customer/project and user choices for dayView & search.
     * 
     * @Route("/choices")
     * @Method("GET")
     * @View()
     * @ApiDoc(
     *   description="GET choices for customer/projects & users (if role manager)"
     * )
     * 
     * @return array
     */
    public function choicesAction()
    {
        $organization = $this->getUser()->getOrganization();
        $em           = $this->getDoctrine()->getManager();

        $data              = [];
        $data['customers'] = $em->getRepository('AppBundle:Customer')->findWithProjects($organization);
        if ($this->get('security.authorization_checker')->isGranted('ROLE_MANAGER')) {
            $data['users'] = $em->getRepository('AppBundle:User')->findByOrganization($organization);
        }

        return $data;
    }

    /**
     * Lists all Hour entities of a specified date.
     * 
     * @Route("/day/{date}")
     * @Method("GET")
     * @View()
     * @ApiDoc(
     *   requirements={
     *      {"name"="date","dataType"="string","requirement"="","description"="date of which you want to see the hour(s) made"},
     *   },
     *   description="GET all hours of specified date"
     * )
     */
    public function dayAction($date)
    {
        $date = new \DateTime($date);
        if (!$date) {
            throw new BadRequestHttpException('Invalid date');
        }

        $em = $this->getDoctrine()->getManager();

        /* @var $repository \AppBundle\Repository\HourRepository */
        $repository = $em->getRepository('AppBundle:Hour');

        /* @var $hours Hour[] */
        $hours = $repository->findByDate($date, $this->getUser());

        return $hours;
    }

    /**
     * Lists all Hour entities of a specified date.
     * 
     * @Route("/search")
     * @Method("POST")
     * @View()
     * @ApiDoc(
     *   parameters={ 
     *      {"name"="search[date_from]","dataType"="string","required"=false,"description"="minimum date in search"},
     *      {"name"="search[date_to]","dataType"="string","required"=false,"description"="max date in search"},
     *      {"name"="search[project]","dataType"="integer","required"=false,"description"="project to search"},
     *      {"name"="search[customer]","dataType"="integer","required"=false,"description"="customer to search"},
     *      {"name"="search[user]","dataType"="integer","required"=false,"description"="user to search"},
     *   },
     *   description="get all hours from a specified filter"
     * )
     */
    public function searchAction(Request $request)
    {
        /* @var $organization AppBundle\Entity\Organization */
        $organization = $this->getUser()->getOrganization();

        $search = new Search();
        $search->setOrganization($organization);

        $form = $this->createForm(SearchType::class, $search);
        $form->handleRequest($request);
        if (!$form->isSubmitted()) {
            throw new BadRequestHttpException("Form was not submitted. Data should be wrapped in formname (ex ['search'=>['project'=>1]]).");
        }
        if (!$form->isValid()) {
            return $form;
        }

        $em = $this->getDoctrine()->getManager();

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_MANAGER')) {
            $search->setUser($this->getUser());
        }

        // save search (not strictly necessary, but interesting)
        $em->persist($search);
        $em->flush();

        /* @var $repository \AppBundle\Repository\HourRepository */
        $repository = $em->getRepository('AppBundle:Hour');

        /* @var $hours Hour[] */
        $hours = $repository->search($search);

        return $hours;
    }

    /**
     * Lists all Hour entities of a specified month grouped by date.
     * 
     * @Route("/month/{year}/{month}")
     * @Method("GET")
     * @View()
     * @ApiDoc(
     *   requirements={
     *      {"name"="month","dataType"="integer","requirement"="","description"="month of which you want to see the hour(s) made"},
     *   },
     *   description="GET all hours of specified month"
     * )
     */
    public function monthAction($month, $year)
    {
        if (!is_numeric($month) && ($month < 1 || $month > 12)) {
            throw new BadRequestHttpException('Invalid month');
        }
        if (!is_numeric($year) && ($year < 1800 || $year > 9999)) {
            throw new BadRequestHttpException('Invalid year');
        }

        $em = $this->getDoctrine()->getManager();

        /* @var $repository \AppBundle\Repository\HourRepository */
        $repository = $em->getRepository('AppBundle:Hour');

        /* @var $hours Hour[] */
        $hours = $repository->findByMonth($this->getUser(), $month, $year);

        return $hours;
    }

    /**
     * Helper function for POST and PATCH.
     *
     * @param Form    $form
     * @param Request $request
     *
     * @return Form
     *
     * @throws BadRequestHttpException
     */
    protected function handleForm(Form $form, Request $request, Hour $hour)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $organization AppBundle\Entity\Organization */
        $organization = $this->getUser()->getOrganization();

        $form->handleRequest($request);

        if (!$form->isSubmitted()) {
            throw new BadRequestHttpException("Form was not submitted. Data should be wrapped in formname (ex ['hour'=>['comment'=>'test']]).");
        }
        if (!$form->isValid()) {
            return $form;
        }

        // check for new customer and use form to validate
        $data = $request->get('hour');
        // unit test breaks cuz it gives $data as Hour object..
        if ($data instanceof Hour) {
            $data = ['new_customer' => $data->getNewCustomer(), 'new_project' => $data->getNewProject()];
        }

        // check for new customer
        if (!empty($data['new_customer'])) {
            $data['new_customer'] = trim($data['new_customer']);

            // check if new customer does not already exist.
            $customer = $em->getRepository('AppBundle:Customer')->findOneBy([
                'organization' => $organization,
                'name'         => $data['new_customer'],
            ]);
            if ($customer === null) {
                $customer = new Customer();
                $customer->setOrganization($organization);
                $customer->setName($data['new_customer']);

                $validator = $this->get('validator');
                $errors    = $validator->validate($customer);
                if (count($errors) > 0) {
                    return ['errors' => ['children' => ['new_customer' => $errors]]];
                }
                $em->persist($customer);
            }
            $hour->setCustomer($customer);
        }

        // check for new project and use form to validate
        if (!empty($data['new_project'])) {
            $data['new_project'] = trim($data['new_project']);

            // check if new project does not already exist.
            $project = $em->getRepository('AppBundle:Project')->findOneBy([
                'organization' => $organization,
                'customer'     => $hour->getCustomer(),
                'name'         => $data['new_project'],
            ]);
            if ($project === null) {
                $project = new Project();
                $project->setOrganization($organization);
                $project->setName($data['new_project']);
                $project->setCustomer($hour->getCustomer());

                $validator = $this->get('validator');
                $errors    = $validator->validate($project);
                if (count($errors) > 0) {
                    return ['errors' => ['children' => ['new_project' => $errors]]];
                }
                $em->persist($project);
            }
            $hour->setProject($project);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($hour);
        $em->flush();
    }

    /**
     * Creates a new Hour entity.
     *
     * @Route("/", name="hour_new")
     * @Method("POST")
     * @View()
     * @ApiDoc(
     *   parameters={ 
     *      {"name"="hour[hours]","dataType"="float","required"=true,"description"="new hour(s)"},
     *      {"name"="hour[date]","dataType"="string","required"=true,"description"="date when you made the hour(s)"},
     *      {"name"="hour[project]","dataType"="string","required"=true,"description"="project for which you made the hour(s)"},
     *      {"name"="hour[customer]","dataType"="string","required"=true,"description"="customer for whom you made the hour(s)"},
     *      {"name"="hour[comment]","dataType"="string","required"=false,"description"="(optional) description of what you did"},
     *      {"name"="hour[new_project]","dataType"="string","required"=true,"description"="(optional) new project by name"},
     *      {"name"="hour[new_customer]","dataType"="string","required"=true,"description"="(optional) new customer by name"}
     *   },
     *   description="POST new hour"
     * )
     */
    public function postAction(Request $request)
    {
        /* @var $organization AppBundle\Entity\Organization */
        $organization = $this->getUser()->getOrganization();

        $hour = new Hour();
        $hour->setOrganization($organization);
        $hour->setUser($this->getUser());

        $form   = $this->createForm(HourType::class, $hour);
        $errors = $this->handleForm($form, $request, $hour);
        if ($errors) {
            return $errors;
        }

        return $hour;
    }

    /**
     * Finds and displays a Hour entity.
     *
     * @Route("/{id}", name="hour_show")
     * @Method("GET")
     * @View()
     * @ApiDoc(
     *   requirements={ { "name"="id","dataType"="integer","requirement"="\d+","description"="id of hour" } },
     *   description="GET single hour by id"
     * )
     */
    public function showAction(Hour $hour)
    {
        return $hour;
    }

    /**
     * Displays a form to edit an existing Hour entity.
     *
     * @Route("/{id}", name="hour_edit")
     * @Method("PATCH")
     * @View()
     * @ApiDoc(
     *   requirements={ { "name"="id","dataType"="integer","requirement"="\d+","description"="id of hour" } },
     *   parameters={ 
     *      {"name"="hour[hours]","dataType"="float","required"=true,"description"="new hour(s)"},
     *      {"name"="hour[date]","dataType"="string","required"=true,"description"="date when you made the hour(s)"},
     *      {"name"="hour[project]","dataType"="string","required"=true,"description"="project for which you made the hour(s)"},
     *      {"name"="hour[customer]","dataType"="string","required"=true,"description"="customer for whom you made the hour(s)"},
     *      {"name"="hour[comment]","dataType"="string","required"=false,"description"="(optional) description of what you did"},
     *      {"name"="hour[new_project]","dataType"="string","required"=true,"description"="(optional) new project by name"},
     *      {"name"="hour[new_customer]","dataType"="string","required"=true,"description"="(optional) new customer by name"}
     *   },
     *   description="PATCH single hour by id"
     * )
     */
    public function patchAction(Request $request, Hour $hour)
    {
        if ($hour->getUser() !== $this->getUser() && !$this->get('security.authorization_checker')->isGranted('ROLE_MANAGER')) {
            throw new AccessDeniedHttpException("User tried to edit someone else's hour");
        }

        $form   = $this->createForm(HourType::class, $hour, ['method' => 'PATCH']);
        $errors = $this->handleForm($form, $request, $hour);
        if ($errors) {
            return $errors;
        }

        return $hour;
    }

    /**
     * Deletes a Hour entity.
     *
     * @Route("/{id}", name="hour_delete")
     * @Method("DELETE")
     * @View()
     * @ApiDoc(
     *   requirements={ { "name"="id","dataType"="integer","requirement"="\d+","description"="id of hour" } },
     *   description="DELETE single Hour by id"
     * )
     */
    public function deleteAction(Request $request, Hour $hour)
    {
        if ($hour->getUser() !== $this->getUser() && !$this->get('security.authorization_checker')->isGranted('ROLE_MANAGER')) {
            throw new AccessDeniedHttpException("User tried to edit someone else's hour");
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($hour);
        $em->flush();

        return true;
    }
}
