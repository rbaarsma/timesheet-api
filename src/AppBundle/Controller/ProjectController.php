<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Entity\Project;
use AppBundle\Form\ProjectType;

/**
 * Project controller.
 *
 * @Route("/project")
 */
class ProjectController extends FOSRestController
{
    /**
     * Lists all Project entities.
     *
     * @Route("/", name="project_index")
     * @Method("GET")
     * @View()
     * @ApiDoc(
     *   description="GET all projects"
     * )
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        /* @var $organization AppBundle\Entity\Organization */
        $organization = $this->get('security.token_storage')->getToken()->getUser()->getOrganization();
        
        $projects = $em->getRepository('AppBundle:Project')->findBy(['organization'=>$organization], ['name'=>'ASC']);

        return $projects;
    }

    /**
     * Creates a new Project entity.
     *
     * @Route("/", name="project_new")
     * @Method("POST")
     * @View()
     * @ApiDoc(
     *   parameters={ 
     *      {"name"="project[name]","dataType"="string","required"=true,"description"="new project name"},
     *      {"name"="project[customer]","dataType"="string","required"=true,"description"="customer id"} 
     *   },
     *   description="POST new project"
     * )
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function postAction(Request $request)
    {
        /* @var $organization AppBundle\Entity\Organization */
        $organization = $this->get('security.token_storage')->getToken()->getUser()->getOrganization();

        $project = new Project();
        $project->setOrganization($organization);

        $form    = $this->createForm(ProjectType::class, $project);
        $form->handleRequest($request);
        
        if (!$form->isSubmitted()) {
            throw new BadRequestHttpException("Form was not submitted. Data should be wrapped in formname (ex ['hour'=>['comment'=>'test']]).");
        }
        if (!$form->isValid()) {
            return $form;
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($project);
        $em->flush();

        return $project;
    }

    /**
     * Finds and displays a Project entity.
     *
     * @Route("/{id}", name="project_show")
     * @Method("GET")
     * @ParamConverter()
     * @View()
     * @ApiDoc(
     *   requirements={ { "name"="id","dataType"="integer","requirement"="\d+","description"="id of project" } },
     *   description="GET single project by id"
     * )
     */
    public function showAction(Project $project)
    {
        return $project;
    }

    /**
     * Displays a form to edit an existing Project entity.
     *
     * @Route("/{id}", name="project_edit")
     * @Method("PATCH")
     * @Security("has_role('ROLE_MANAGER')")
     * @View()
     * @ApiDoc(
     *   requirements={ { "name"="id","dataType"="integer","requirement"="\d+","description"="id of project" } },
     *   parameters={ 
     *      {"name"="project[name]","dataType"="string","required"=true,"description"="new project name"},
     *      {"name"="project[customer]","dataType"="string","required"=true,"description"="customer id"} 
     *   },
     *   description="PATCH single project by id"
     * )
     */
    public function patchAction(Request $request, Project $project)
    {
        $form = $this->createForm(ProjectType::class, $project, ['method' => 'PATCH']);

        $form->handleRequest($request);
        if (!$form->isSubmitted()) {
            throw new BadRequestHttpException("Form was not submitted. Data should be wrapped in formname (ex ['hour'=>['comment'=>'test']]).");
        }
        if (!$form->isValid()) {
            return $form;
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $project;
    }

    /**
     * Deletes a Project entity.
     *
     * @Route("/{id}", name="project_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_MANAGER')")
     * @View()
     * @ApiDoc(
     *   requirements={ { "name"="id","dataType"="integer","requirement"="\d+","description"="id of project" } },
     *   description="DELETE single Project by id"
     * )
     */
    public function deleteAction(Request $request, Project $project)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($project);
        $em->flush();

        return true;
    }
}
