<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Entity\User;
use AppBundle\Form\UserPostType;
use AppBundle\Form\UserType;
use AppBundle\Entity\Organization;
use AppBundle\Form\OrganizationType;

/**
 * User controller.
 *
 * @Route("/user")
 */
class UserController extends FOSRestController
{
    /**
     * Register an organization & first admin user
     * 
     * @Method("POST")
     * @Route("/register")
     * @ApiDoc(
     *   parameters={
     *      {"name"="user[email]","dataType"="string","required"=true,"description"="Valid e-mail"},
     *      {"name"="user[plainPassword]","dataType"="string","required"=true,"description"="Unencrypted password."},
     *      {"name"="user[first_name]","dataType"="string","required"=true,"description"="First name"},
     *      {"name"="user[last_name]","dataType"="string","required"=true,"description"="Last name."},
     *      {"name"="organization[name]","dataType"="string","required"=true,"description"="Organization name"},
     *      {"name"="organization[phone]","dataType"="string","required"=true,"description"="(optional) Phone number"},
     *      {"name"="organization[website]","dataType"="string","required"=true,"description"="(optional) Website url"}
     *   },
     *   description="Register organization + first admin user"
     * )
     */
    public function registerAction(Request $request)
    {
        // first register organization by organization form
        $organization = new Organization();
        $form = $this->createForm(OrganizationType::class, $organization);
        $form->handleRequest($request);
        if (!$form->isSubmitted()) {
            throw new BadRequestHttpException("Form was not submitted. Data should contain organization form (ex ['organization'=>['name'=>'test']]).");
        }
        if (!$form->isValid()) {
            return $form;
        }
    
        // then register user with new organization
        $user = new User();
        $user->setOrganization($organization);
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        $user->setUsername($user->getEmail());
        if (!$form->isSubmitted()) {
            throw new BadRequestHttpException("Form was not submitted. Data should be wrapped in formname (ex ['user'=>['email'=>'test@solidwebcode.com', 'plainPassword'=>'test']]).");
        }
        if (!$form->isValid()) {
            return $form;
        }

        // overwrite whatever was set on roles/enabled
        $user->setRoles(['ROLE_ADMIN']);
        $user->setEnabled(true);        
        
        // insert into the database
        $em = $this->getDoctrine()->getManager();
        $em->persist($organization);
        $em->persist($user);
        $em->flush();

        return $user;
    }
    
    /**
     * @Method("POST")
     * @Route("/send-reset")
     * @ApiDoc(
     *   parameters={
     *      {"name"="email","dataType"="string","required"=true,"description"="Your e-mail"}
     *   },
     *   description="Send a resetting link to a specific e-mail address"
     * )
     */
    public function sendResetAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $email = $request->get('email');
        if (empty($email)) {
            throw new BadRequestHttpException("E-mail is required");
        }

        /** @var User $user */
        $user = $em->getRepository('AppBundle:User')->findOneByEmail($email);        
        if ($user === null) {
            throw $this->createNotFoundException("Reset could not find user by e-mail: $email");
        }

        if (false === $user->isEnabled()) {
            throw new BadRequestHttpException("This account has been disabled.");
        }

        // set confirmation token for reset to identify user
        $user->setConfirmationToken(sha1(uniqid("",true)));
        $em->flush();
        
        // all is ok, sent e-mail
        $message = \Swift_Message::newInstance()
            ->setSubject('Password Reset')
            ->setFrom('solidtimesheet@solidwebcode.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                    'emails/reset-password.html.twig',
                    array('token' => $user->getConfirmationToken())
                ),
                'text/html'
            )
        ;
        $this->get('mailer')->send($message);
        
        return true;
    }
    
    /**
     * @Method("POST")
     * @Route("/reset")
     * @ApiDoc(
     *   parameters={
     *      {"name"="token","dataType"="string","required"=true,"description"="Confirm e-mail token"},
     *      {"name"="plainPassword","dataType"="string","required"=true,"description"="New password"}
     *   },
     *   description="Change your password from a resetting password link/token"
     * )
     */    
    public function resetAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $token = $request->get('token');
        if (!$token) {
            throw new BadRequestHttpException("An e-mail confirmation token is required.");
        }
        
        $user = $em->getRepository('AppBundle:User')->findOneByConfirmationToken($token);

        if ($user === null) {
            throw $this->createNotFoundException('Illegal reset request: no such confirmation token');
        }
        
        $user->setConfirmationToken(null);
        $user->setPlainPassword($request->get('plainPassword'));
        
        $validator = $this->get('validator');
        $errors = $validator->validate($user);
        
        if (count($errors) > 0) {
            return $errors;
        }
        
        $em->flush();
        
        // login user with jwt token

        /* @var $jwt_manager Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager */
        $jwt_manager = $this->get('lexik_jwt_authentication.jwt_manager');
        $jwt  = $jwt_manager->create($user);

        $response = new \Symfony\Component\HttpFoundation\JsonResponse();
        $event    = new AuthenticationSuccessEvent(['token' => $jwt], $user, $request, $response);

        $this->get('event_dispatcher')->dispatch(Events::AUTHENTICATION_SUCCESS, $event);
        $token_arr = $event->getData();
            
        // return user & token
        return [
            'token' => $token_arr['token'],
            'user' => $user
        ];
    }    
    
    /**
     * Note this function doesn't actually do something as loginCheck is handled
     * by the security layer, but it makes sure it's in the ApiDoc
     * 
     * @Method("POST")
     * @Route("/login_check")
     * @ApiDoc(
     *   parameters={
     *      {"name"="_username","dataType"="string","required"=true,"description"="Username (user@localhost or admin@localhost)"},
     *      {"name"="_password","dataType"="string","required"=true,"description"="Password (1234)"}
     *   },
     *   description="GET login token"
     * )
     */
    public function loginCheck() {}
    
    /**
     * Lists all User entities.
     *
     * @Security("has_role('ROLE_MANAGER')")
     * @Route("/", name="user_index")
     * @Method("GET")
     * @View()
     * @ApiDoc(
     *   description="GET all users"
     * )
     */
    public function indexAction()
    {        
        $em = $this->getDoctrine()->getManager();

        /* @var $repository \AppBundle\Repository\UserRepository */
        $repository = $em->getRepository('AppBundle:User');

        /* @var $users User[] */
        $users = $repository->findByOrganization($this->getUser()->getOrganization());

        return $users;
    }

    /**
     * Creates a new User entity.
     *
     * @Security("has_role('ROLE_MANAGER')")
     * @Route("/")
     * @Method("POST")
     * @ApiDoc(
     *   parameters={ 
     *      {"name"="user[email]","dataType"="string","required"=true,"description"="Valid e-mail"},
     *      {"name"="user[plainPassword]","dataType"="string","required"=true,"description"="Unencrypted password."},
     *      {"name"="user[enabled]","dataType"="boolean","required"=false,"description"="Wether or not the user should be locked. Default: false"},
     *      {"name"="user[first_name]","dataType"="string","required"=true,"description"="First name"},
     *      {"name"="user[last_name]","dataType"="string","required"=true,"description"="Last name."},
     *      {"name"="user[roles]","dataType"="array","required"=false,"description"="Special user roles, such as ROLE_ADMIN or ROLE_MANAGER"}
     *   },
     *   requirements={ { "name"="id","dataType"="integer","requirement"="\d+","description"="id of user" } },
     *   description="POST user"
     * )

     */
    public function postAction(Request $request)
    {
        /* @var $organization AppBundle\Entity\Organization */
        $organization = $this->getUser()->getOrganization();
    
        $user = new User();
        $user->setOrganization($organization);
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        $user->setUsername($user->getEmail());
        if (!$form->isSubmitted()) {
            throw new BadRequestHttpException("Form was not submitted. Data should be wrapped in formname (ex ['user'=>['email'=>'test@solidwebcode.com', 'plainPassword'=>'test']]).");
        }
        if (!$form->isValid()) {
            return $form;
        }
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $user;
    }

    /**
     * Finds and displays a User entity.
     *
     * @Route("/{id}", requirements={"id" = "\d+"}, name="user_show")
     * @Method("GET")
     * @ParamConverter()
     * @View()
     * @ApiDoc(
     *   requirements={ { "name"="id","dataType"="integer","requirement"="\d+","description"="id of user" } },
     *   description="GET single user by id"
     * )
     */
    public function showAction(User $user)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_MANAGER')
            && $this->getUser()->getId() !== $user->getId()) {
            throw new AccessDeniedHttpException("Only ROLE_MANAGER can edit users not your own.");
        }
        
        return $user;
    }

    /**
     * Finds and displays a User entity.
     *
     * @Route("/me")
     * @Method("GET")
     * @View()
     * @ApiDoc(
     *   description="GET your own user data"
     * )
     */
    public function meAction()
    {
        return $this->getUser();
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}", name="user_edit")
     * @Method("PATCH")
     * @View()
     * @ApiDoc(
     *   requirements={ { "name"="id","dataType"="integer","requirement"="\d+","description"="id of user" } },
     *   parameters={ 
     *      {"name"="user[email]","dataType"="string","required"=true,"description"="Valid e-mail"},
     *      {"name"="user[plainPassword]","dataType"="string","required"=true,"description"="Unencrypted password."},
     *      {"name"="user[locked]","dataType"="boolean","required"=false,"description"="Wether or not the user should be locked. Default: false"},
     *      {"name"="user[first_name]","dataType"="string","required"=true,"description"="First name"},
     *      {"name"="user[last_name]","dataType"="string","required"=true,"description"="Last name."},
     *      {"name"="user[roles]","dataType"="array","required"=false,"description"="Special user roles, such as ROLE_ADMIN or ROLE_MANAGER"} 
     *   },
     *   description="PATCH single user by id"
     * )
     */
    public function patchAction(Request $request, User $user)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_MANAGER')
            && $this->getUser()->getId() !== $user->getId()) {
            throw new AccessDeniedHttpException("Only ROLE_MANAGER can edit users not your own.");
        }
        
        $form = $this->createForm(UserPostType::class, $user, array('method' => 'PATCH'));

        $form->handleRequest($request);
        $user->setUsername($user->getEmail());
        if (!$form->isSubmitted()) {
            throw new BadRequestHttpException("Form was not submitted. Data should be wrapped in formname (ex ['user'=>['email'=>'test@solidwebcode.com', 'plainPassword'=>'test']]).");
        }
        if (!$form->isValid()) {
            return $form;
        }
        
        $em = $this->getDoctrine()->getManager();
        $em->flush();        

        return $user;
    }

    /**
     * Deletes a User entity.
     *
     * @Security("has_role('ROLE_MANAGER')")
     * @Route("/{id}", name="user_delete")
     * @Method("DELETE")
     * @View()
     * @ApiDoc(
     *   requirements={ { "name"="id","dataType"="integer","requirement"="\d+","description"="id of user" } },
     *   description="DELETE single User by id"
     * )
     */
    public function deleteAction(Request $request, User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();        

        return true;
    }
}
