<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Entity\Customer;
use AppBundle\Form\CustomerType;

/**
 * @Route("/customer")
 * @ApiDoc(
 *   requirements={{"name"="version","dataType"="integer","requirement"="\d+","description"="api version number (not used yet)"}},
 *   description="Customer API"
 * )
 */
class CustomerController extends FOSRestController
{
    /**
     * @Route("/")
     * @Method("GET")
     * @View()
     * @ApiDoc(
     *   description="GET all customers"
     * )
     */
    public function allAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /* @var $organization AppBundle\Entity\Organization */
        $organization = $this->get('security.token_storage')->getToken()->getUser()->getOrganization();

        $customers = $em->getRepository('AppBundle:Customer')->findBy(['organization' => $organization], ['name' => 'ASC']);

        return $customers;
    }

    /**
     * @Route("/{id}")
     * @Method("GET")
     * @ParamConverter()
     * @View()
     * @ApiDoc(
     *   requirements={ {"name"="id","dataType"="integer","requirement"="\d+","description"="id of customer"} },
     *   description="GET single customer by id"
     * )
     */
    public function getAction(Request $request, Customer $customer)
    {
        return $customer;
    }

    /**
     * @Route("/{id}")
     * @Method("DELETE")
     * @Security("has_role('ROLE_MANAGER')")
     * @ParamConverter()
     * @View()
     * @ApiDoc(
     *   requirements={ {"name"="id","dataType"="integer","requirement"="\d+","description"="id of customer"} },
     *   description="DELETE single customer by id"
     * )
     */
    public function deleteAction(Request $request, Customer $customer)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($customer);
        $em->flush();

        return true;
    }

    /**
     * @Route("/")
     * @Method("POST")
     * @View()
     * @Security("has_role('ROLE_MANAGER')")
     * @ApiDoc(
     *  parameters={ 
     *      {"name"="customer[name]","dataType"="string","required"=true,"description"="new customer"}
     *  },
     *   description="POST new customer"
     * )
     */
    public function postAction(Request $request)
    {
        /* @var $organization AppBundle\Entity\Organization */
        $organization = $this->get('security.token_storage')->getToken()->getUser()->getOrganization();

        $customer = new Customer();
        $customer->setOrganization($organization);
        $form = $this->createForm(CustomerType::class, $customer);
        $form->handleRequest($request);
        if (!$form->isSubmitted()) {
            throw new BadRequestHttpException("Form was not submitted. Data should be wrapped in formname (ex ['hour'=>['comment'=>'test']]).");
        }
        if (!$form->isValid()) {
            return $form;
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($customer);
        $em->flush();

        return $customer;
    }

    /**
     * @Route("/{id}")
     * @Method("PATCH")
     * @ParamConverter()
     * @Security("has_role('ROLE_MANAGER')")
     * @View()
     * @ApiDoc(
     *   requirements={ {"name"="id","dataType"="integer","requirement"="\d+","description"="id of customer"} },
     *   parameters={ {"name"="customer[name]","dataType"="string","required"=true,"description"="new customer name"} },
     *   description="PATCH single customer by id"
     * )
     */
    public function patchAction(Request $request, Customer $customer)
    {
        $form = $this->createForm(CustomerType::class, $customer, ['method' => 'PATCH']);
        $form->handleRequest($request);
        if (!$form->isSubmitted()) {
            throw new BadRequestHttpException("Form was not submitted. Data should be wrapped in formname (ex ['hour'=>['comment'=>'test']]).");
        }
        if (!$form->isValid()) {
            return $form;
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $customer;
    }
}
