<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class EqualsUserOrganization extends Constraint
{
    public $message = "The organization for this entity does not match your logged in user's organization.";

    public $message_not_an_entity   = 'This column is not a ManyToOne or OneToOne relation and therefore cannot be checked by this validator.';
    public $message_no_organization = 'This entity has no organization. The validator cannot work for this entity.';
    public $message_no_valid_user   = "There doesn't seem to be any logged in user.";

    public function validatedBy()
    {
        return 'equals_user_organization';
    }
}
