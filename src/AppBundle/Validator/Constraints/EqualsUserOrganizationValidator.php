<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use AppBundle\Entity\User;

class EqualsUserOrganizationValidator extends ConstraintValidator
{
    private $token_storage;

    /**
     * Constructor.
     *
     * @param TokenStorage $ts
     */
    public function __construct(TokenStorage $ts)
    {
        $this->token_storage = $ts;
    }

    /**
     * Validates if the entity.
     *
     * @param type       $entity     Entity class
     * @param Constraint $constraint
     */
    public function validate($entity, Constraint $constraint)
    {
        if ($entity === null) {
            return;
        }

        if (false === is_object($entity)) {
            $this->context->buildViolation($constraint->message_not_an_entity)->addViolation();
        }

        if (false === method_exists($entity, 'getOrganization')) {
            $this->context->buildViolation($constraint->message_no_organization)->addViolation();
        }

        $user = $this->token_storage->getToken()->getUser();
        if ($user === null || !($user instanceof User)) {
            $this->context->buildViolation($constraint->message_no_valid_user)->addViolation();
        }

        if ($entity->getOrganization() !== $user->getOrganization()) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
