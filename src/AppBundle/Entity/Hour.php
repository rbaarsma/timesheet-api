<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as AppAssert;
use JMS\Serializer\Annotation as JMS;

/**
 * Hour.
 *
 * @ORM\Table(name="hour")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HourRepository")
 */
class Hour
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Organization
     *
     * @ORM\ManyToOne(targetEntity="Organization")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=false)
     * @JMS\Exclude()
     */
    protected $organization;

    /**
     * @var User
     *
     * @AppAssert\EqualsUserOrganization
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=false)
     */
    private $user;

    /**
     * @var Project
     * 
     * @AppAssert\EqualsUserOrganization
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project")
     * @ORM\JoinColumn(onDelete="SET NULL", nullable=true)
     */
    private $project;

    /**
     * @var Customer
     * 
     * @AppAssert\EqualsUserOrganization
     * @Assert\Expression(
     *     expression="this.getNewCustomer() != '' || value != ''",
     *     message="Either customer or new_customer must be set"
     * )
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Customer")
     * @ORM\JoinColumn(onDelete="SET NULL", nullable=true)
     */
    private $customer;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     * @Assert\Range(min=0, max=24)
     */
    private $hours;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     * @JMS\Type("DateTime<'d-m-Y'>")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;
    
    /**
     * Helper for new project
     * @var string
     * @JMS\Exclude()
     */
    private $new_project;
    
    /**
     * Helper for new customer
     * @var string
     * @JMS\Exclude()
     */
    private $new_customer;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     * @JMS\Exclude()
     */
    private $created_at;
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->created_at = new \DateTime();
    }
    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hours.
     *
     * @param float $hours
     *
     * @return Hour
     */
    public function setHours($hours)
    {
        $this->hours = $hours;

        return $this;
    }

    /**
     * Get hours.
     *
     * @return float
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return Hour
     */
    public function setDate (\DateTime $date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set comment.
     *
     * @param string $comment
     *
     * @return Hour
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set organization.
     *
     * @param \AppBundle\Entity\Organization $organization
     *
     * @return Hour
     */
    public function setOrganization(\AppBundle\Entity\Organization $organization = null)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get organization.
     *
     * @return \AppBundle\Entity\Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Set user.
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Hour
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set project.
     *
     * @param \AppBundle\Entity\Project $project
     *
     * @return Hour
     */
    public function setProject(\AppBundle\Entity\Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project.
     *
     * @return \AppBundle\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set customer.
     *
     * @param \AppBundle\Entity\Customer $customer
     *
     * @return Hour
     */
    public function setCustomer(\AppBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer.
     *
     * @return \AppBundle\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set newProject
     *
     * @param string $newProject
     *
     * @return Hour
     */
    public function setNewProject($newProject)
    {
        $this->new_project = $newProject;

        return $this;
    }

    /**
     * Get newProject
     *
     * @return string
     */
    public function getNewProject()
    {
        return $this->new_project;
    }

    /**
     * Set newCustomer
     *
     * @param string $newCustomer
     *
     * @return Hour
     */
    public function setNewCustomer($newCustomer)
    {
        $this->new_customer = $newCustomer;

        return $this;
    }

    /**
     * Get newCustomer
     *
     * @return string
     */
    public function getNewCustomer()
    {
        return $this->new_customer;
    }
}
