<?php

namespace AppBundle;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpKernel\Client;

class AppTestCase extends WebTestCase
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var string
     */
    protected $token;

    /**
     * @return Client
     */
    protected function getClient()
    {
        return isset($this->client) ? $this->client : $this->client = $client = static::createClient();
    }

    /**
     * Shortcut to login with username/password. Simply remembers token for jsonRequest
     * Note: defaults to basic user.
     * 
     * @param string $username
     * @param string $password
     */
    protected function login($username = 'user@localhost', $password = '1234')
    {
        $client = $this->getClient();
        $client->request('POST', '/api/user/login_check', [
            '_username' => $username,
            '_password' => $password,
        ]);
        $data = json_decode($client->getResponse()->getContent());
        if (!isset($data->token)) {
            throw new \Exception('No token found in login response: '.print_r($data, true));
        }
        $this->token = $data->token;
    }

    /**
     * Shorthand function for doing correct api request.
     * 
     * @param string $method
     * @param string $url
     * @param array  $data
     * @param int    $expected_status
     *
     * @return array $json_data
     */
    protected function jsonRequest($method, $url, array $data = null, $expected_status = 200)
    {
        $client = $this->getClient();

        $header = ['CONTENT_TYPE' => 'application/json'];
        if (isset($this->token)) {
            $header['HTTP_AUTHORIZATION'] = 'Bearer '.$this->token;
        }
        $json = $data !== null ? json_encode($data) : null;
        $url  = '/api'.$url;

        $client->request($method, $url, [], [], $header, $json);
        if ($expected_status !== $client->getResponse()->getStatusCode()) {
            echo $client->getResponse();
        }
        $this->assertEquals($expected_status, $client->getResponse()->getStatusCode());

        return $data = json_decode($client->getResponse()->getContent());
    }
}
