<?php

namespace AppBundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use AppBundle\Form\UserType;

/**
 * Basically add validation group POST
 */
class UserPostType extends UserType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'validation_groups' => ['POST']
        ]);
    }
    
    /**
     * Keep it simple, since we distinguish the forms with POST and PATCH already
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'user';
    }
}
