<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Organization;

/**
 * CustomerRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CustomerRepository extends \Doctrine\ORM\EntityRepository
{
    public function findWithProjects(Organization $organization)
    {
        return $this->createQueryBuilder('q')
            ->select('q,p')
            ->join('q.projects', 'p')
            ->where('q.organization = :organization')
            ->setParameter('organization', $organization)
            ->getQuery()
            ->getResult()
        ;
    }
}
