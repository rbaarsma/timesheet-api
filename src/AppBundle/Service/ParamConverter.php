<?php

namespace AppBundle\Service;

use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\DoctrineParamConverter;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Bridge\Doctrine\ManagerRegistry;

/**
 * Child of DoctrineParamConverter which automatically checks if objects are
 * not used within the wrong scope (wrong theme).
 */
class ParamConverter extends DoctrineParamConverter
{
    /**
     * @var TokenStorage
     */
    protected $token_storage;

    /**
     * Constructor.
     * 
     * @param ManagerRegistry $registry
     * @param TokenStorage    $ts
     */
    public function __construct(ManagerRegistry $registry, TokenStorage $ts)
    {
        parent::__construct($registry);
        $this->token_storage = $ts;
    }

    /**
     * @return \AppBundle\Entity\User
     */
    protected function getUser()
    {
        if (null === $token = $this->token_storage->getToken()) {
            return;
        }

        if (!is_object($user = $token->getUser())) {
            return;
        }

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    protected function find($class, Request $request, $options, $name)
    {
        $obj = parent::find($class, $request, $options, $name);
        $this->checkObject($obj);

        return $obj;
    }

    /**
     * {@inheritdoc}
     */
    protected function findOneBy($class, Request $request, $options)
    {
        $obj = parent::findOneBy($class, $request, $options);
        $this->checkObject($obj);

        return $obj;
    }

    /**
     * Helper function to check object integrity (Theme).
     * 
     * @param object $obj
     *
     * @throws AccessDeniedHttpException
     */
    protected function checkObject($obj)
    {
        if (method_exists($obj, 'getOrganization')) {
            if ($obj->getOrganization() !== $this->getUser()->getOrganization()) {
                throw new AccessDeniedHttpException('Wrong organization: object is not from this organization');
            }
        }
    }
}
