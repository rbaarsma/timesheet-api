<?php

namespace AppBundle\Service;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\User;

class UserProvider implements UserProviderInterface
{
    public $em;
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    
    
    public function loadUserByUsername($username)
    {
        $user = $this->em->getRepository('AppBundle:User')->loadUserByUsername($username);
        if ($user)
            return $user;
        
        throw new UsernameNotFoundException(
            sprintf('Username "%s" does not exist.', $username)
        );        
    }
    
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }
        
        $user = $this->em->getRepository('AppBundle:User')->find($user->getId());
        return $user;
    }

    public function supportsClass($class)
    {
        return $class === 'AppBundle\Entity\User';
    }
}